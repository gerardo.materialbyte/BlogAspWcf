﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference1;

namespace ProyectoFinalCliente1
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ServiceReference1.BlogServiceClient proxy = new ServiceReference1.BlogServiceClient("blog");
            mostrarBlogsConMasLikes(proxy);
            mostrarBlogsConMasVistas(proxy);
        }

        void mostrarBlogsConMasLikes(ServiceReference1.BlogServiceClient proxy)
        {
            List<Blog> blogs = new List<Blog>();
            foreach (var item in proxy.blogsByLikes())
            {
                blogs.Add(item);
            }
            LVBlogsLikes.DataSource = blogs;
            LVBlogsLikes.DataBind();
        }

        void mostrarBlogsConMasVistas(ServiceReference1.BlogServiceClient proxy)
        {
            List<Blog> blogs = new List<Blog>();
            foreach (var item in proxy.blogsByViews())
            {
                blogs.Add(item);
            }
            LVBlogsVistas.DataSource = blogs;
            LVBlogsVistas.DataBind();
        }

        protected void btnIrBlog_Click(object sender, EventArgs e)
        {
            string texto = (sender as Button).Text.ToString();
            Response.Redirect("BlogView.aspx?blog="+texto);
        }

        protected void LVBlogsVistas_SelectedIndexChanging(object sender, ListViewSelectEventArgs e)
        {
            //ListViewItem item = (ListViewItem)LVBlogsLikes.Items[e.NewSelectedIndex];
            //Label lb = (Label)item.FindControl("lbl1");
            //Response.Redirect("BlogView.aspx?blog=" + lb.Text);
        }
    }
}