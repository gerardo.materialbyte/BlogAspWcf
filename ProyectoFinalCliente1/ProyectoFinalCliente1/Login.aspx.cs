﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference2;

namespace ProyectoFinalCliente1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            ServiceReference2.Service1Client proxy = new ServiceReference2.Service1Client("usuario");
            Usuario usuario = proxy.login(txtUsuario.Text);
            if (usuario.pPassword == txtPassword.Text)
            {
                Session["userId"] = usuario.pId.ToString();
                Response.Redirect("Default.aspx");
            } else
            {
                Response.Redirect("Registrarse.aspx");
            }
        }
    }
}