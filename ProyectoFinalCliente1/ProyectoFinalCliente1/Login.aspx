﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ProyectoFinalCliente1.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="row">
        <div class="col s12 m3 offset-m4 card">
            <div class="row">
                <div class="input-field col s12">
                  <asp:TextBox ID="txtUsuario" CssClass="validate" runat="server"></asp:TextBox>
                  <label for="username">Username</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="validate"></asp:TextBox>
                  <label for="password">Password</label>
                </div>
            </div>
            <div class="row">
                <div class="s12 m12">
                    <asp:Button CssClass="waves-effect waves-light btn" ID="btnRegistrar" runat="server" Text="Ingresar" OnClick="btnRegistrar_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
