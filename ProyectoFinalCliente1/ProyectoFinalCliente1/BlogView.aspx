﻿<%@ Page Title="BlogView.aspx" EnableEventValidation="false" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BlogView.aspx.cs" Inherits="ProyectoFinalCliente1.BlogView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="row">
        <div class="col s12 m8 offset-m2 card">
            <div class="card-content">
                <div class="row"><h1 align="center"><asp:Label ID="lblTitulo" CssClass="center-align" style="text-transform:uppercase" runat="server"></asp:Label></h1></div>
                <div class="row"><h5><asp:Label ID="lblContenido" runat="server"></asp:Label></h5></div>
                <div class="row">
                    <div class="col s4 m4" align="center">
                        <i class="small material-icons">thumb_up</i> &nbsp; <asp:Label ID="lblLikes" runat="server" Font-Size="small"></asp:Label>
                    </div>
                    <div class="col s4 m4" align="center">
                        <i class="small material-icons">done_all</i> &nbsp;<asp:Label ID="lblVistas" runat="server" Font-Size="small"></asp:Label>
                    </div>
                    <div class="col s4 m4" align="center">
                        <i class="small material-icons">date_range</i> &nbsp; <asp:Label ID="lblFechaCreacion" runat="server" Font-Size="small"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 m8 offset-m2">
            <div class="row">
                <div class="col s12 m6 center-align">
                    <div class="card">
                        <div class="card-content">
                            <img src="imagenes/perfil.jpeg" alt="" class="circle" height="100" width="100">
                            <h5>Autor</h5>
                            <h6 runat="server" id="lblNombreAutor" style="text-transform:uppercase;"></h6>
                            <h6 runat="server" id="lblAutor" class="blue-text"></h6>
                        </div>
                    </div>
                </div>
                <div class="col s12 m6 center-align">
                    <div class="card">
                        <div class="card-content">
                            <h5>Categorias</h5>
                            <asp:ListView ID="Lvcategorias" runat="server">
                                <ItemTemplate>
                                    <div class="chip">
                                        <%#Eval("pNombre") %>
                                    </div>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col s12 m6 offset-m3">
            <ul class="collection with-header">
                <li class="collection-header"><h4>Comentarios</h4></li>
                    <asp:ListView ID="LvlComentarios" runat="server">
                        <ItemTemplate>
                            <li class="collection-item avatar">
                                <div>
                                    <img src="imagenes/perfil.jpeg" height="50" width="50" class="circle"/>
                                    <h5><%#Eval("pContenido") %></h5>
                                    <h6 class="secondary-content"><%#Eval("pFechaCreacion") %></h6>
                                    <h6 class="blue-text">@<%#Eval("pUsername") %></h6>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:ListView>
                </ul>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m6 offset-m3 card" runat="server" visible="false" id="tarjetaComentario">
            <div class="row">
                <div class="input-field col s12">
                    <textarea id="txtContenido" runat="server" class="materialize-textarea" cols="20" rows="20"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="s12 m12">
                    <asp:Button CssClass="waves-effect waves-light btn" ID="btnComentar" runat="server" Text="Agregar Comentario" OnClick="btnComentar_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
