﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference2;
using ProyectoFinalCliente1.ServiceReference1;

namespace ProyectoFinalCliente1
{
    public partial class Perfil : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                obtenerPerfil();
                cargarGrid();
            }
        }

        void obtenerPerfil()
        {
            if (Session["userId"] != null)
            {
                string nombre = Session["userId"].ToString();
                ServiceReference2.Service1Client proxy = new ServiceReference2.Service1Client("usuario");
                Usuario usuario = new Usuario();
                usuario = proxy.userById(int.Parse(nombre));
                lblApellido.Text = usuario.pApellido;
                lblNombre.Text = usuario.pNombre;
                lblEstado.Text = usuario.pEstado;
                lblUsername.Text = usuario.pUsername;
            }
            else
            {
                
            }
        }

        void cargarGrid()
        {
            if (Session["userId"] != null)
            {
                ServiceReference1.BlogServiceClient proxy = new ServiceReference1.BlogServiceClient("blog");
                List<Blog> blogs = new List<Blog>();
                int idAutor = int.Parse(Session["userId"].ToString());
                foreach (var item in proxy.blogsByAuthor(idAutor))
                {
                    blogs.Add(item);
                }
                GridViewBlogs.DataSource = blogs;
                GridViewBlogs.DataBind();
            }
        }

        protected void GridViewBlogs_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            int fila = e.NewSelectedIndex;
            Response.Redirect("BlogView.aspx?blog=" + GridViewBlogs.Rows[fila].Cells[0].Text);
        }

        protected void btnCrearBlog_Click(object sender, EventArgs e)
        {
            Response.Redirect("CrearBlog.aspx");
        }

        protected void GridViewBlogs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ServiceReference1.BlogServiceClient proxy = new ServiceReference1.BlogServiceClient("blog");
            string id = GridViewBlogs.Rows[e.RowIndex].Cells[0].Text;
            proxy.eliminarBlog(int.Parse(id));
            Response.Redirect(Request.RawUrl);
        }

        protected void btnRegistrarBlog_Click(object sender, EventArgs e)
        {
            if (Session["userId"] != null)
            {
                Response.Redirect("RegistrarBlogs.aspx");
            }
        }
    }
}