﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Perfil.aspx.cs" Inherits="ProyectoFinalCliente1.Perfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="row">
        <div class="col s12 m3 offset-m1">
            <div class="card">
                <div class="card-content">
                    <img class="circle center-align" src="imagenes/perfil.jpeg" width="150" height="150"/>
                </div>
            </div>
            <div class="card">
                <div class="card-content">
                    <ul>
                        <li>
                            <span class="blue-text">NOMBRE:</span> &nbsp;<asp:Label ID="lblNombre" style="text-transform:uppercase" runat="server"></asp:Label>
                        </li>
                        <li>
                            <span class="blue-text">APELLIDO:</span> &nbsp;<asp:Label ID="lblApellido"  style="text-transform:uppercase" runat="server"></asp:Label>
                        </li>
                        <li>
                            <span class="blue-text">USERNAME:</span> &nbsp;<asp:Label ID="lblUsername"  style="text-transform:uppercase" runat="server"></asp:Label>
                        </li>
                        <li>
                            <span class="blue-text">ESTADO:</span>&nbsp;<asp:Label ID="lblEstado"  style="text-transform:uppercase" runat="server"></asp:Label>
                        </li>
                        <li>
                            <br />
                            <asp:Button ID="btnCrearBlog" OnClick="btnCrearBlog_Click" CssClass="waves-effect waves-light btn-large" runat="server" Text="Crear Nuevo Blog" />
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col s12 m8">
            <div class="card">
                <div class="card-content">
                    <asp:GridView ID="GridViewBlogs" runat="server" 
                AutoGenerateColumns="False" OnSelectedIndexChanging="GridViewBlogs_SelectedIndexChanging" OnRowDeleting="GridViewBlogs_RowDeleting">
                        <Columns>
                            <asp:BoundField DataField="pId" HeaderText="id"/>
                            <asp:BoundField DataField="pTitulo" HeaderText="Titulo" />
                            <asp:BoundField DataField="pLikes" HeaderText="Likes" />
                            <asp:BoundField DataField="pVistas" HeaderText="Vistas" />
                            <asp:CommandField ButtonType="Button" HeaderText="Ver" 
                                ShowSelectButton="True" ControlStyle-CssClass="waves-effect btn" SelectText="Ver">
                            </asp:CommandField>
                            <asp:CommandField ButtonType="Button" HeaderText="Eliminar" SelectText="Eliminar" 
                                 ShowDeleteButton="true" ControlStyle-CssClass="waves-effect btn" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
