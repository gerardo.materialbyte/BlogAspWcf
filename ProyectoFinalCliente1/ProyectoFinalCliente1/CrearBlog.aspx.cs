﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference1;
using System.ServiceModel;
using System.Transactions;

namespace ProyectoFinalCliente1
{
    public partial class CrearBlog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnCrear_Click(object sender, EventArgs e)
        {
            using (TransactionScope ts = new TransactionScope(TransactionScopeOption.Required))
            {
                try
                {
                    ServiceReference1.BlogServiceClient proxy = new ServiceReference1.BlogServiceClient("blog");
                    Blog blog = new Blog();
                    blog.pIdAutor = int.Parse(Session["userId"].ToString());
                    blog.pTitulo = txtTitulo.Value;
                    blog.pContenido = txtContenido.Value;
                    blog.pFechaCreacion = DateTime.Now;
                    blog.pEstado = "activo";
                    blog.pLikes = 0;
                    blog.pVistas = 0;
                    proxy.createBlog(blog);
                    ts.Complete();
                    Response.Redirect("RegistrarBlogs.aspx");
                }
                catch (FaultException<BlogFoult> e1)
                {
                    BlogFoult bf = e1.Detail;
                    lblMensaje.InnerText = bf.pCodError + " " + bf.pDetailError;
                    message.Visible = true;
                    ts.Dispose();
                }
                catch (FaultException e2)
                {
                    lblMensaje.InnerText = e2.Reason.ToString();
                    message.Visible = true;
                    ts.Dispose();
                }
                catch (Exception e3)
                {
                    lblMensaje.InnerText = e3.Message;
                    message.Visible = true;
                    ts.Dispose();
                }
            }
        }
    }
}