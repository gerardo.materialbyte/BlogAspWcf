﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Registrarse.aspx.cs" Inherits="ProyectoFinalCliente1.Registrarse" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col s12 m6 offset-m3 card">
          <div class="row">
            <div class="input-field col s6">
                <input id="txtNombre" type="text" class="validate" runat="server" required>
                <label for="first_name">First Name</label>
            </div>
            <div class="input-field col s6">
                <input id="txtApellido" type="text" class="validate" runat="server" required>
                <label for="last_name">Last Name</label>
            </div>
           </div>
            <div class="row">
                <div class="input-field col s12">
                  <input id="txtUsername" type="text" class="validate" runat="server" required>
                  <label for="username">Username</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                  <input id="txtPassword" type="password" class="validate" runat="server" required>
                  <label for="password">Password</label>
                </div>
            </div>
            <div class="row">
                <div class="s12 m12">
                    <asp:Button CssClass="waves-effect waves-light btn" ID="btnRegistrar" OnClick="btnRegistrar_Click" runat="server" Text="Registrar" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
