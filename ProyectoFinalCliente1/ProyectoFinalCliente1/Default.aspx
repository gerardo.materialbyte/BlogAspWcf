﻿<%@ Page Title="Home Page" EnableEventValidation="false" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ProyectoFinalCliente1._Default" %>
<asp:Content ID="header" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
<h1 align="center">Blogs Con mas Likes</h1>
    <div class="row">
        <div class="col s12 m8 offset-m2">
            <asp:ListView ID="LVBlogsLikes" runat="server">
                <ItemTemplate>
                    <div class="col s12 m4">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="imagenes/sample-1.jpg">
                            </div>
                            <div class="card-content">
                                <span class="card-title grey-text text-darken-4"><%#Eval("pTitulo") %></span>
                                <br />
                                <div class="row">
                                    <div class="col s6 m6 center-align">
                                        <i class="small material-icons">thumb_up</i> &nbsp; <%#Eval("pLikes") %>
                                    </div>
                                    <div class="col s6 m6 center-align">
                                        <i class="small material-icons">done_all</i> &nbsp;<%#Eval("pVistas") %>
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:Button CssClass="btn waves-light waves-effect waves-block" runat="server" 
                                            OnClick="btnIrBlog_Click" Text='<%#Eval("pId") %>'> </asp:Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
    <hr />
    <h1 align="center">Blogs Con mas Vistas</h1>
        <div class="row">
            <div class="col s12 m8 offset-m2">
                <asp:ListView ID="LVBlogsVistas" runat="server" OnSelectedIndexChanging="LVBlogsVistas_SelectedIndexChanging">
                    <ItemTemplate>
                        <div class="col s12 m4">
                            <div class="card">
                                <div class="card-image waves-effect waves-block waves-light">
                                    <img class="activator" src="imagenes/sample-1.jpg">
                                </div>
                                <div class="card-content">
                                    <span class="card-title grey-text text-darken-4"><%#Eval("pTitulo") %></span>
                                    <br />
                                    <div class="row">
                                        <div class="col s6 m6 center-align">
                                            <i class="small material-icons">thumb_up</i> &nbsp; <%#Eval("pLikes") %>
                                        </div>
                                        <div class="col s6 m6 center-align">
                                            <i class="small material-icons">done_all</i> &nbsp;<%#Eval("pVistas") %>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <asp:Button CssClass="btn waves-light waves-effect waves-block" runat="server" 
                                                OnClick="btnIrBlog_Click" Text='<%#Eval("pId") %>'> </asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
</asp:Content>
