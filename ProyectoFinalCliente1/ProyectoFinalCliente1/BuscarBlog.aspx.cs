﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference1;

namespace ProyectoFinalCliente1
{
    public partial class BuscarBlog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            ServiceReference1.BlogServiceClient proxy = new ServiceReference1.BlogServiceClient("blog");
            List<Blog> blogs = new List<Blog>();
            foreach (var item in proxy.searchBlog(txtSearch.Value.ToString()))
            {
                blogs.Add(item);
            }
            LVBlogsEncontrados.DataSource = blogs.ToList();
            LVBlogsEncontrados.DataBind();
        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            string texto = (sender as Button).Text.ToString();
            Response.Redirect("BlogView.aspx?blog=" + texto);
        }
    }
}