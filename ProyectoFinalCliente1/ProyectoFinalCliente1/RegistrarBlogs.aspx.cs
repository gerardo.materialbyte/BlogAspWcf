﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference1;
using ProyectoFinalCliente1.ServiceReference4;

namespace ProyectoFinalCliente1
{
    public partial class RegistrarBlogs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["userId"] != null)
                {
                    ServiceReference4.CategoriaServiceClient proxy1 = new ServiceReference4.CategoriaServiceClient("categoria");
                    List<ServiceReference4.Categoria> categorias = new List<ServiceReference4.Categoria>();
                    foreach (var item in proxy1.obtenerCategorias())
                    {
                        categorias.Add(item);
                    }
                    CheckBoxList1.DataSource = categorias;
                    CheckBoxList1.DataTextField = "pNombre";
                    CheckBoxList1.DataValueField = "pId";
                    CheckBoxList1.DataBind();
                }
            }
        }

        protected void btnAgregarCategoria_Click(object sender, EventArgs e)
        {
            ServiceReference1.BlogServiceClient proxy = new ServiceReference1.BlogServiceClient("blog");
            Blog blog = new Blog();
            int idAutor = int.Parse(Session["userId"].ToString());
            blog = proxy.ultimoBlogCreadoPorAutor(idAutor);

            ServiceReference4.CategoriaServiceClient proxy1 = new ServiceReference4.CategoriaServiceClient("categoria");

            foreach (ListItem item in CheckBoxList1.Items)
            {
                if (item.Selected)
                    proxy1.registrarBlogACategoria(blog.pId, int.Parse(item.Value.ToString()));
                    //System.Diagnostics.Debug.WriteLine(blog.pId+"hola"+item.Value.ToString());
                    
            }
            Response.Redirect("Perfil.aspx");
        }
    }
}