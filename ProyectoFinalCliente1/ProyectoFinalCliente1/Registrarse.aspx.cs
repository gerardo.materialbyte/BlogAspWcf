﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference2;

namespace ProyectoFinalCliente1
{
    public partial class Registrarse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            ServiceReference2.Service1Client proxy = new ServiceReference2.Service1Client("usuario");
            Usuario usuario = new Usuario();
            usuario.pNombre = txtNombre.Value;
            usuario.pApellido = txtApellido.Value;
            usuario.pUsername = txtUsername.Value;
            usuario.pPassword = txtPassword.Value;
            usuario.pEstado = "activo";
            if (proxy.login(usuario.pUsername) == null)
            {
                Usuario user = new Usuario();
                proxy.createUser(usuario);
                user = proxy.login(usuario.pUsername);
                Session["userId"] = user.pId.ToString();
                if (Session["userId"] != null)
                {
                    Response.Redirect("Default.aspx");
                }
            } else
            {
                Response.Redirect(Request.RawUrl);
            }
        }
    }
}