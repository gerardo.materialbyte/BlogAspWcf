﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProyectoFinalCliente1
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["userId"] != null)
                {
                    lblPerfil.Visible = true;
                    lblPErfil1.Visible = true;
                    lblLogin.Visible = false;
                    lblLogin1.Visible = false;
                    btnCerrarSesion.Visible = true;
                    btnCerrarSesion1.Visible = true;
                    lblRegistrarse.Visible = false;
                    lblRegistrarse1.Visible = false;
                }
            }
        }

        protected void btn1_click(object sender, EventArgs e)
        {
            if (Session["userId"] != null)
            {
                Session["userId"] = null;
                lblLogin.Visible = true;
                lblLogin1.Visible = true;
                lblPerfil.Visible = false;
                lblPerfil.Visible = false;
                btnCerrarSesion.Visible = false;
                btnCerrarSesion1.Visible = false;
                Response.Redirect("Default.aspx");
            }
        }

        protected void btn2_click(object sender, EventArgs e)
        {
            if (Session["userId"] != null)
            {
                Session["userId"] = null;
                lblLogin.Visible = true;
                lblLogin1.Visible = true;
                lblPerfil.Visible = false;
                lblPerfil.Visible = false;
                btnCerrarSesion.Visible = false;
                btnCerrarSesion1.Visible = false;
                Response.Redirect("Default.aspx");
            }
        }
    }
}