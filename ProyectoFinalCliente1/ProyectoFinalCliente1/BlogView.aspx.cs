﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProyectoFinalCliente1.ServiceReference3;
using ProyectoFinalCliente1.ServiceReference1;
using ProyectoFinalCliente1.ServiceReference2;
using ProyectoFinalCliente1.ServiceReference4;

namespace ProyectoFinalCliente1
{
    public partial class BlogView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                cargarBlog();
                cargarComentarios();
                cargarCategorias();
            }
        }

        void cargarBlog()
        {
            if (Request["blog"] != null)
            {
                ServiceReference1.BlogServiceClient proxy = new ServiceReference1.BlogServiceClient("blog");
                Blog blog = new Blog();
                int idBlog = int.Parse(Request["blog"].ToString());
                blog = proxy.blogById(idBlog);
                if (blog != null)
                {
                    lblTitulo.Text = blog.pTitulo;
                    lblContenido.Text = blog.pContenido;
                    lblFechaCreacion.Text = blog.pFechaCreacion.ToShortDateString();
                    lblLikes.Text = blog.pLikes.ToString();
                    lblVistas.Text = blog.pVistas.ToString();

                    ServiceReference2.Service1Client proxyUser = new ServiceReference2.Service1Client("usuario");
                    Usuario user = proxyUser.userById(blog.pIdAutor);
                    lblNombreAutor.InnerText = user.pNombre + " " + user.pApellido;
                    lblAutor.InnerText = "@" + user.pUsername;
                }

                if (Session["userId"] != null) {
                    tarjetaComentario.Visible = true;
                }
            }
        }

        void cargarComentarios()
        {
            ServiceReference3.ComentariosServiceClient proxy = new ServiceReference3.ComentariosServiceClient("comentario");
            List<Comentario> comentarios = new List<Comentario>();
            if (Request["blog"] != null)
            {
                int idBlog = int.Parse(Request["blog"].ToString());
                foreach (var item in proxy.comentariosByBlog(idBlog))
                {
                    comentarios.Add(item);
                }
                LvlComentarios.DataSource = comentarios.ToList();
                LvlComentarios.DataBind();
            }
        }

        void cargarCategorias()
        {
            ServiceReference4.CategoriaServiceClient proxy = new ServiceReference4.CategoriaServiceClient("categoria");
            List<ServiceReference4.Categoria> lista = new List<ServiceReference4.Categoria>();
            if(Request["blog"] != null)
            {
                int blog = int.Parse(Request["blog"].ToString());
                foreach (var item in proxy.obtenerCategoriasPorBlog(blog))
                {
                    lista.Add(item);
                }

                Lvcategorias.DataSource = lista.ToList();
                Lvcategorias.DataBind();
            }
        }

        protected void btnComentar_Click(object sender, EventArgs e)
        {
            ServiceReference3.ComentariosServiceClient proxy = new ServiceReference3.ComentariosServiceClient("comentario");
            Comentario comentario = new Comentario();
            if (Session["userId"] != null)
            {
                comentario.pIdAutor = int.Parse(Session["userId"].ToString());
                comentario.pIdBlog = int.Parse(Request["blog"].ToString());
                comentario.pEstado = "activo";
                comentario.pContenido = txtContenido.Value;
                comentario.pFechaCreacion = DateTime.Now;
                proxy.crear(comentario);
                Response.Redirect(Request.RawUrl);
            }
        }
    }
}