﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CrearBlog.aspx.cs" Inherits="ProyectoFinalCliente1.CrearBlog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <div class="row">
          <div class="col s12 m6 offset-m3 card">
              <div class="card-content">
                  <div class="row">
                    <div class="input-field col s12">
                      <input id="txtTitulo" runat="server" type="text" class="validate">
                      <label for="titulo">Titulo</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <textarea id="txtContenido" class="materialize-textarea" rows="20" runat="server"></textarea>
                      <label for="contenido">Contenido</label>
                    </div>
                  </div>
                  <asp:Button ID="btnCrear" CssClass="waves-effect waves-light btn" OnClick="btnCrear_Click" runat="server" Text="Crear" />
              </div>
          </div>
      </div>

    <div class="row" runat="server" visible="false" id="message">
        <div class="col s12 m6 offset-m3">
            <div class="card-panel teal">
                <span class="white-text" runat="server" id="lblMensaje"></span>
            </div>
        </div>
    </div>
</asp:Content>
