﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BuscarBlog.aspx.cs" Inherits="ProyectoFinalCliente1.BuscarBlog" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="row">
        <div class="col s12 m4 offset-m4">
            <div class="row">
                <div class="col s12 m8">
                    <nav class="#00897b teal darken-1">
                        <div class="nav-wrapper">
                            <div class="input-field">
                                <input id="txtSearch" runat="server" type="search" required>
                                <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                <i class="material-icons">close</i>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="col s12 m4">
                    <asp:Button runat="server" ID="btnBuscar" Text="Buscar" OnClick="btnBuscar_Click" CssClass="waves-effect waves-light btn-large" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s12 m8 offset-m2">
            <asp:ListView ID="LVBlogsEncontrados" runat="server">
                <ItemTemplate>
                    <div class="col s12 m4">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <img class="activator" src="imagenes/sample-1.jpg">
                            </div>
                            <div class="card-content">
                                <span class="card-title grey-text text-darken-4"><%#Eval("pTitulo") %></span>
                                <br />
                                <div class="row">
                                    <div class="col s6 m6 center-align">
                                        <i class="small material-icons">thumb_up</i> &nbsp; <%#Eval("pLikes") %>
                                    </div>
                                    <div class="col s6 m6 center-align">
                                        <i class="small material-icons">done_all</i> &nbsp;<%#Eval("pVistas") %>
                                    </div>
                                </div>
                                <div class="row">
                                    <asp:Button CssClass="btn waves-light waves-effect waves-block" runat="server" 
                                             OnClick="Unnamed_Click" Text='<%#Eval("pId") %>'> </asp:Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</asp:Content>
