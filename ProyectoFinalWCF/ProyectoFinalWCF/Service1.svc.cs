﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ProyectoFinalWCF.models;
using System.Data.SqlClient;
using System.Data;

namespace ProyectoFinalWCF
{
    public class Service1 : IService1
    {
        SqlConnection cnx = new SqlConnection();
        
        public Service1()
        {
            cnx.ConnectionString = @"Data Source=DESKTOP-TABFPCA;Initial Catalog=BDPROYECTO;integrated security=True";
        }
        public Usuario createUser(Usuario user)
        {
            try
            {
                SqlCommand sql = new SqlCommand("insert into Usuario values ('" + user.pNombre + "','" + user.pApellido + "','" + user.pUsername + "','" + user.pPassword + "','" + user.pEstado + "')", cnx);
                cnx.Open();
                sql.ExecuteNonQuery();
                cnx.Close();

                return user;
            }
            catch (Exception)
            {
                throw new Exception("No se pudo crear el usuario");
            }
        }

        public Usuario login(string username)
        {
            
            SqlCommand sql = new SqlCommand("select * from Usuario where username = '" + username + "'", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Usuario> lista = new List<Usuario>();
            while (dr.Read())
            {
                lista.Add(convertirUsuario(dr));
            }
            cnx.Close();
            return lista.FirstOrDefault();
        }

        public Usuario userById(int id)
        {
            SqlCommand sql = new SqlCommand("select * from Usuario where id = "+id, cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Usuario> lista = new List<Usuario>();
            while (dr.Read())
            {
                lista.Add(convertirUsuario(dr));
            }
            cnx.Close();
            return lista.FirstOrDefault();
        }

        private static Usuario convertirUsuario(IDataReader reader)
        {
            Usuario usuario = new Usuario();
            usuario.pId = Convert.ToInt32(reader["id"]);
            usuario.pNombre = Convert.ToString(reader["nombre"]);
            usuario.pApellido = Convert.ToString(reader["apellido"]);
            usuario.pUsername = Convert.ToString(reader["username"]);
            usuario.pPassword = Convert.ToString(reader["password"]);
            usuario.pEstado = Convert.ToString(reader["estado"]);

            return usuario;
        }
    }
}
