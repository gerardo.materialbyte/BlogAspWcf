﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ProyectoFinalWCF.models;

namespace ProyectoFinalWCF
{
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        Usuario createUser(models.Usuario user);

        [OperationContract]
        Usuario userById(int id);

        [OperationContract]
        Usuario login(string username); 
    }
}
