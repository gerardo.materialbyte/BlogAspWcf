﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ProyectoFinalWCF.models
{
    [DataContract]
    public class Categoria
    {
        private int id;
        private string nombre;
        private string estado;

        [DataMember]
        public int pId
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string pNombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        [DataMember]
        public string pEstado
        {
            get { return estado; }
            set { estado = value; }
        }

    }
}