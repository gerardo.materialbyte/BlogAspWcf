﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ProyectoFinalWCF.models
{
    [DataContract]
    public class CategoriaBlog
    {
        private int id;
        private int idCategoria;
        private int idBlog;

        [DataMember]
        public int pId
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public int pIdCategoria
        {
            get { return idCategoria; }
            set { idCategoria = value; }
        }

        [DataMember]
        public int pIdBlog
        {
            get { return idBlog; }
            set { idBlog = value; }
        }
    }
}