﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ProyectoFinalWCF.models
{
    [DataContract]
    public class Blog
    {
        private int id;
        private string titulo;
        private int idAutor;
        private DateTime fechaCreacion;
        private string contenido;
        private int likes;
        private int vistas;
        private string estado;

        [DataMember]
        public int pId
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string pTitulo
        {
            get { return titulo; }
            set { titulo = value; }
        }

        [DataMember]
        public int pIdAutor
        {
            get { return idAutor; }
            set { idAutor = value; }
        }

        [DataMember]
        public DateTime pFechaCreacion
        {
            get { return fechaCreacion; }
            set { fechaCreacion = value; }
        }

        [DataMember]
        public string pContenido
        {
            get { return contenido; }
            set { contenido = value; }
        }

        [DataMember]
        public int pLikes
        {
            get { return likes; }
            set { likes = value; }
        }

        [DataMember]
        public int pVistas
        {
            get { return vistas; }
            set { vistas = value; }
        }

        [DataMember]
        public string pEstado
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}