﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ProyectoFinalWCF.models
{
    [DataContract]
    public class Comentario
    {
        private int id;
        private int idAutor;
        private string username;
        private int idBlog;
        private string contenido;
        private string estado;
        private DateTime fechaCreacion;

        [DataMember]
        public int pId
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public int pIdAutor
        {
            get { return idAutor; }
            set { idAutor = value; }
        }

        [DataMember]
        public string pUsername
        {
            get { return username; }
            set { username = value; }
        }

        [DataMember]
        public int pIdBlog
        {
            get { return idBlog; }
            set { idBlog = value; }
        }

        [DataMember]
        public string pContenido
        {
            get { return contenido; }
            set { contenido = value; }
        }

        [DataMember]
        public string pEstado
        {
            get { return estado; }
            set { estado = value; }
        }

        [DataMember]
        public DateTime pFechaCreacion
        {
            get { return fechaCreacion; }
            set { fechaCreacion = value; }
        }
    }
}