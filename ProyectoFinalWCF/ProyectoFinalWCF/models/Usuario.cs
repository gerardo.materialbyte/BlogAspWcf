﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ProyectoFinalWCF.models
{
    [DataContract]
    public class Usuario
    {
        private int id;
        private string nombre;
        private string apellido;
        private string username;
        private string password;
        private string estado;

        [DataMember]
        public int pId
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string pNombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        [DataMember]
        public string pApellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        [DataMember]
        public string pUsername
        {
            get { return username; }
            set { username = value; }
        }

        [DataMember]
        public string pPassword
        {
            get { return password; }
            set { password = value; }
        }

        [DataMember]
        public string pEstado
        {
            get { return estado; }
            set { estado = value; }
        }
    }
}