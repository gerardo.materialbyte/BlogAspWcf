﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ProyectoFinalWCF.models
{
    [DataContract]
    public class BlogFoult
    {
        private int codError;
        private int codBlog;
        private string nombre;
        private string detailError;

        public BlogFoult()
        {
            codError = 0;
            codBlog = 0;
            nombre = "";
            detailError = "No definido";
        }

        [DataMember]
        public int pCodError
        {
            get { return codError; }
            set { codError = value; }
        }

        [DataMember]
        public int pCodBlog
        {
            get { return codBlog; }
            set { codBlog = value; }
        }

        [DataMember]
        public string pNombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        [DataMember]
        public string pDetailError
        {
            get { return detailError; }
            set { detailError = value; }
        }
    }
}