﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProyectoFinalWCF.models;
using System.Data;
using System.Data.SqlClient;

namespace ProyectoFinalWCF.Servicios
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "CategoriaService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione CategoriaService.svc o CategoriaService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class CategoriaService : ICategoriaService
    {
        SqlConnection cnx = new SqlConnection();
        public CategoriaService()
        {
            cnx.ConnectionString = @"Data Source=DESKTOP-TABFPCA;Initial Catalog=BDPROYECTO;integrated security=True";
        }
        public bool registrarBlogACategoria(int idBlog, int idCategoria)
        {
            try
            {
                SqlCommand sql = new SqlCommand("insert into CategoriaBlog values ("+idCategoria+","+idBlog+")", cnx);
                cnx.Open();
                sql.ExecuteNonQuery();
                cnx.Close();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("No se pudo crear el Comentario");
            }
        }

        public List<Categoria> obtenerCategorias()
        {
            SqlCommand sql = new SqlCommand("select * from Categoria", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Categoria> lista = new List<Categoria>();
            while (dr.Read())
            {
                lista.Add(convertirCategoria(dr));
            }
            cnx.Close();
            return lista.ToList();
        }

        private static Categoria convertirCategoria(IDataReader reader)
        {
            Categoria categoria = new Categoria();
            categoria.pId = Convert.ToInt32(reader["id"]);
            categoria.pNombre = Convert.ToString(reader["nombre"]);
            categoria.pEstado = Convert.ToString(reader["estado"]);

            return categoria;
        }

        public List<Categoria> obtenerCategoriasPorBlog(int idBlog)
        {
            SqlCommand sql = new SqlCommand("select Categoria.id, nombre, estado from CategoriaBlog inner join Categoria on Categoria.id = CategoriaBlog.idCategoria where idBlog = "+idBlog, cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Categoria> lista = new List<Categoria>();
            while (dr.Read())
            {
                lista.Add(convertirCategoria(dr));
            }
            cnx.Close();
            return lista.ToList();
        }
    }
}
