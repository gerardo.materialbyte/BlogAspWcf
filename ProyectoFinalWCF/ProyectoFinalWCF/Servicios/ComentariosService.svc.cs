﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProyectoFinalWCF.models;
using System.Data;
using System.Data.SqlClient;

namespace ProyectoFinalWCF.Servicios
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ComentariosService" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ComentariosService.svc o ComentariosService.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ComentariosService : IComentariosService
    {
        SqlConnection cnx = new SqlConnection();

        public ComentariosService()
        {
            cnx.ConnectionString = @"Data Source=DESKTOP-TABFPCA;Initial Catalog=BDPROYECTO;integrated security=True";
        }
        public List<Comentario> comentariosByBlog(int id)
        {
            SqlCommand sql = new SqlCommand("select Comentario.id, Comentario.idAutor, Comentario.idBlog, Comentario.contenido, Comentario.estado, Comentario.fechaCreacion, Usuario.username from Comentario inner join Usuario on Comentario.idAutor = Usuario.id where Comentario.idBlog = " + id+" and Comentario.estado = 'activo'", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Comentario> lista = new List<Comentario>();
            while (dr.Read())
            {
                lista.Add(convertirComentario(dr));
            }
            cnx.Close();
            return lista.ToList();
        }

        public bool eliminarComentario(int id)
        {
            try
            {
                SqlCommand sql = new SqlCommand("update Comentario set estado = 'inactivo'", cnx);
                cnx.Open();
                sql.ExecuteNonQuery();
                cnx.Close();
                return true;
            }
            catch (Exception)
            {
                throw new Exception("No se pudo crear el Comentario");
            }
        }

        private static Comentario convertirComentario(IDataReader reader)
        {
            Comentario comentario = new Comentario();
            comentario.pId = Convert.ToInt32(reader["id"]);
            comentario.pIdAutor = Convert.ToInt32(reader["idAutor"]);
            comentario.pUsername = Convert.ToString(reader["username"]);
            comentario.pIdBlog = Convert.ToInt32(reader["idBlog"]);
            comentario.pContenido = Convert.ToString(reader["contenido"]);
            comentario.pEstado = Convert.ToString(reader["estado"]);
            comentario.pFechaCreacion = Convert.ToDateTime(reader["fechaCreacion"]);

            return comentario;
        }

        public void crear(Comentario comentario)
        {
            try
            {
                SqlCommand sql = new SqlCommand("insert into Comentario values ("+comentario.pIdAutor+","+comentario.pIdBlog+",'"+comentario.pContenido+"', '"+comentario.pEstado+"','"+comentario.pFechaCreacion+"')", cnx);
                cnx.Open();
                sql.ExecuteNonQuery();
                cnx.Close();
            }
            catch (Exception)
            {
                throw new Exception("No se pudo crear el Comentario");
            }
        }
    }
}
