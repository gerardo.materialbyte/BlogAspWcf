﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProyectoFinalWCF.models;
using System.Net.Security;

namespace ProyectoFinalWCF.Servicios
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IBlogService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IBlogService
    {
        [TransactionFlow(TransactionFlowOption.Mandatory)]
        [OperationContract(ProtectionLevel =ProtectionLevel.None)]
        [FaultContract(typeof(BlogFoult))]
        bool createBlog(Blog blog);

        [OperationContract]
        Blog blogById(int id);

        [OperationContract]
        List<Blog> searchBlog(string criteria);

        [OperationContract]
        List<Blog> blogsByLikes();

        [OperationContract]
        List<Blog> blogsByViews();

        [OperationContract]
        List<Blog> blogsByCategory(Categoria category);

        [OperationContract]
        List<Blog> blogsByAuthor(int id);

        [OperationContract]
        bool eliminarBlog(int id);

        [OperationContract]
        Blog ultimoBlogCreadoPorAutor(int idAutor);
    }
}
