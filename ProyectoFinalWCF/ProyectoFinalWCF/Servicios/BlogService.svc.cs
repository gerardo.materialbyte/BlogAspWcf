﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProyectoFinalWCF.models;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;

namespace ProyectoFinalWCF.Servicios
{
    [ServiceBehavior(TransactionIsolationLevel=System.Transactions.IsolationLevel.Serializable)]
    public class BlogService : IBlogService
    {
        SqlConnection cnx = new SqlConnection();

        public BlogService()
        {
            cnx.ConnectionString = @"Data Source=DESKTOP-TABFPCA;Initial Catalog=BDPROYECTO;integrated security=True";
        }
        public Blog blogById(int id)
        {
            SqlCommand sql = new SqlCommand("select * from Blog where id = " + id + " and estado = 'activo'", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Blog> lista = new List<Blog>();
            while (dr.Read())
            {
                lista.Add(convertirBlog(dr));
            }
            cnx.Close();
            return lista.FirstOrDefault();
        }

        public List<Blog> blogsByCategory(Categoria category)
        {
            /*SqlCommand sql = new SqlCommand("select * from Blog where id = " + id, cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Blog> lista = new List<Blog>();
            while (dr.Read())
            {
                lista.Add(convertirBlog(dr));
            }
            cnx.Close();
            return lista.FirstOrDefault();*/
            throw new NotImplementedException();
        }

        public List<Blog> blogsByLikes()
        {
            SqlCommand sql = new SqlCommand("select top 3 * from Blog where estado = 'activo' order by likes desc", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Blog> lista = new List<Blog>();
            while (dr.Read())
            {
                lista.Add(convertirBlog(dr));
            }
            cnx.Close();
            return lista.ToList();
        }

        public List<Blog> blogsByViews()
        {
            SqlCommand sql = new SqlCommand("select top 3 * from Blog where estado = 'activo' order by vistas desc", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Blog> lista = new List<Blog>();
            while (dr.Read())
            {
                lista.Add(convertirBlog(dr));
            }
            cnx.Close();
            return lista.ToList();
        }

        [OperationBehavior(TransactionScopeRequired =true)]
        public bool createBlog(Blog blog)
        {
            try
            {
                if (blog.pIdAutor < 1)
                    throw new OverflowException("Autor no valido");
                if (blog.pTitulo == "" || blog.pTitulo == null)
                    throw new ArgumentNullException("Titulo del blog vacio");
                if (blog.pContenido == "" || blog.pContenido == null)
                    throw new ArgumentNullException("Contenido del blog vacio");

                SqlCommand sql = new SqlCommand("insert into Blog values ('" + blog.pTitulo + "'," + blog.pIdAutor + ",'" + blog.pFechaCreacion + "','" + blog.pContenido + "'," + blog.pLikes + "," + blog.pVistas + ",'" + blog.pEstado + "')", cnx);
                cnx.Open();
                sql.ExecuteNonQuery();
                cnx.Close();

                return true;
            }
            catch (OverflowException e)
            {
                BlogFoult blogFoult = new BlogFoult();
                blogFoult.pCodError = 401;
                blogFoult.pCodBlog = blog.pId;
                blogFoult.pNombre = blog.pTitulo;
                blogFoult.pDetailError = e.Message;
                throw new FaultException<BlogFoult>(blogFoult);
            }
            catch (ArgumentNullException e1)
            {
                BlogFoult blogFoult = new BlogFoult();
                blogFoult.pCodError = 401;
                blogFoult.pCodBlog = blog.pId;
                blogFoult.pNombre = blog.pTitulo;
                blogFoult.pDetailError = e1.Message;
                throw new FaultException<BlogFoult>(blogFoult);
            }
            catch (Exception e2)
            {
                throw new FaultException(e2.Message);
            }
        }

        public List<Blog> searchBlog(string criteria)
        {
            SqlCommand sql = new SqlCommand("select top 10 Blog.id, Blog.titulo, Blog.idAutor, Blog.fechaCreacion, Blog.contenido, Blog.likes, Blog.vistas, Blog.estado from Blog inner join Usuario on Usuario.id = Blog.idAutor where Blog.estado = 'activo' AND ((Blog.titulo like '%"+criteria+ "%') or (Blog.contenido like '%" + criteria + "%') or (Usuario.username like '%" + criteria + "%') or (Usuario.nombre like '%" + criteria + "%') or (Usuario.apellido like '%" + criteria + "%')) GROUP BY Blog.id, Blog.titulo, Blog.idAutor, Blog.fechaCreacion, Blog.contenido, Blog.likes, Blog.vistas, Blog.estado", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Blog> lista = new List<Blog>();
            while (dr.Read())
            {
                lista.Add(convertirBlog(dr));
            }
            cnx.Close();
            return lista.ToList();
        }

        private static Blog convertirBlog(IDataReader reader)
        {
            Blog blog = new Blog();
            blog.pId = Convert.ToInt32(reader["id"]);
            blog.pTitulo = Convert.ToString(reader["titulo"]);
            blog.pIdAutor = Convert.ToInt32(reader["idAutor"]);
            blog.pFechaCreacion = Convert.ToDateTime(reader["fechaCreacion"]);
            blog.pContenido = Convert.ToString(reader["contenido"]);
            blog.pLikes = Convert.ToInt32(reader["likes"]);
            blog.pVistas = Convert.ToInt32(reader["vistas"]);
            blog.pEstado = Convert.ToString(reader["estado"]);

            return blog;
        }

        public List<Blog> blogsByAuthor(int id)
        {
            SqlCommand sql = new SqlCommand("select top 10 * from Blog where idAutor = "+id+" and estado = 'activo'", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Blog> lista = new List<Blog>();
            while (dr.Read())
            {
                lista.Add(convertirBlog(dr));
            }
            cnx.Close();
            return lista.ToList();
        }

        public bool eliminarBlog(int id)
        {
            try
            {
                SqlCommand sql = new SqlCommand("update Blog set estado = 'inactivo' where id = "+id, cnx);
                cnx.Open();
                sql.ExecuteNonQuery();
                cnx.Close();

                return true;
            }
            catch (Exception)
            {
                throw new Exception("No se pudo crear el Blog");
            }
        }

        public Blog ultimoBlogCreadoPorAutor(int idAutor)
        {
            SqlCommand sql = new SqlCommand("select top 1 * from Blog where idAutor = "+idAutor+" order by id desc", cnx);
            cnx.Open();

            SqlDataReader dr = sql.ExecuteReader();
            List<Blog> lista = new List<Blog>();
            while (dr.Read())
            {
                lista.Add(convertirBlog(dr));
            }
            cnx.Close();
            return lista.FirstOrDefault();
        }
    }
}
