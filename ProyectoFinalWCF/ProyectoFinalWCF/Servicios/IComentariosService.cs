﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ProyectoFinalWCF.models;

namespace ProyectoFinalWCF.Servicios
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IComentariosService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IComentariosService
    {
        [OperationContract]
        List<Comentario> comentariosByBlog(int id);

        [OperationContract]
        bool eliminarComentario(int id);

        [OperationContract]
        void crear(Comentario comentario);
    }
}
